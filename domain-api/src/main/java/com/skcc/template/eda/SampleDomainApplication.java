package com.skcc.template.eda;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class SampleDomainApplication {

	private static final String PROPERTIES =
									"spring.config.location="
									+ "classpath:/config/application/";
		
	public static void main(String[] args) {
		new SpringApplicationBuilder(SampleDomainApplication.class)
            .properties(PROPERTIES)
            .run(args);
	}

}
