package com.skcc.template.eda.gobal.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.util.StopWatch;

import lombok.extern.slf4j.Slf4j;

@Aspect
@Slf4j
public class LogAspect {

    @Around("execution(* com.skcc.template.eda..*.*(..))" 
            + "&& !execution(* com.skcc.template.eda.gobal.config.AopConfig.*(..))"
            + "&& !execution(* com.skcc.template.eda.gobal.config.WebAppConfig.*(..))"
            + "&& !execution(* com.skcc.template.eda.sample_domain.core.domain.entity.SampleDomainAggregate.*(..))" 
            )        
    public Object logging(ProceedingJoinPoint pjp) throws Throwable {

        log.debug("[{}.{} Called]", pjp.getTarget().getClass().getSimpleName(), pjp.getSignature().getName());
        Object[] args = pjp.getArgs();
        for( Object arg : args){
            //log.debug("==>(Argument.{}) {}", arg.getClass().getSimpleName(), (arg == null)? "null" : arg.toString() );
        }

        StopWatch sw = new StopWatch();
        sw.start();
        
        Object result = pjp.proceed();

        sw.stop();
        //Long totalProceedTime = sw.getTotalTimeMillis();

        //log.debug("[{}.{} Finished] Proceed Time: {} (ms)", pjp.getTarget().getClass().getSimpleName(), pjp.getSignature().getName(), totalProceedTime);
        //log.debug("==>(Return.{}) {}", (result == null)? "null" : result.getClass().getSimpleName(), (result == null)? "null" : result.toString() );

        return result;
    }

}