package com.skcc.template.eda.gobal.config;

import com.skcc.template.eda.gobal.aop.LogAspect;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile({"local","dev","stg"})
@EnableAspectJAutoProxy
public class AopConfig {

    @Bean 
    public LogAspect logAspect(){
        return new LogAspect();
    }

}