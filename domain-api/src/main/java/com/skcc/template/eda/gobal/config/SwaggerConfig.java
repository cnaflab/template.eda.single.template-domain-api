package com.skcc.template.eda.gobal.config;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.classmate.TypeResolver;
import com.skcc.template.eda.gobal.error.ErrorResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.client.LinkDiscoverer;
import org.springframework.hateoas.client.LinkDiscoverers;
import org.springframework.hateoas.mediatype.collectionjson.CollectionJsonLinkDiscoverer;
import org.springframework.plugin.core.SimplePluginRegistry;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@EnableWebMvc
public class SwaggerConfig {

    @Autowired
    private TypeResolver typeResolver;

    @Bean
    public Docket swaggerSpringMvcPluginWeb() {

        ArrayList<ResponseMessage> responseMessages = new ArrayList<ResponseMessage>();

        ResponseMessage responseMessage400 = new ResponseMessageBuilder().code(400)
                        //.message("500 message")
                        .responseModel(new ModelRef("ErrorResponse"))
                        .build();
        ResponseMessage responseMessage401 = new ResponseMessageBuilder().code(401)
                        .responseModel(new ModelRef("ErrorResponse"))
                        .build();
        ResponseMessage responseMessage404 = new ResponseMessageBuilder().code(404)
                        .responseModel(new ModelRef("ErrorResponse"))
                        .build();

        responseMessages.add(responseMessage400);
        responseMessages.add(responseMessage401);
        responseMessages.add(responseMessage404);

        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Rest API")
                .additionalModels(typeResolver.resolve(ErrorResponse.class))
                .globalResponseMessage(RequestMethod.GET, responseMessages)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.skcc.template.eda.sample_domain.controller.web"))
                .build()
                .apiInfo(restApiInfo());
    }

    @Bean
    public Docket swaggerSpringMvcPluginMessage() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Message API")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.skcc.template.eda.sample_domain.controller.handler"))
                .build()
                .apiInfo(messageApiInfo());
    }

    @Bean
    public LinkDiscoverers discoverers() {
        List<LinkDiscoverer> plugins = new ArrayList<>();
        plugins.add(new CollectionJsonLinkDiscoverer());
        return new LinkDiscoverers(SimplePluginRegistry.create(plugins));

    }

    private ApiInfo restApiInfo() {
        return new ApiInfoBuilder()
            .title("SAMPLE Rest API")
            .description("DESCRIPTION")
            .version("VERSION")
            .termsOfServiceUrl("http://terms-of-services.url")
            .license("LICENSE")
            .licenseUrl("http://url-to-license.com")
            .build();
    }

    private ApiInfo messageApiInfo() {
        return new ApiInfoBuilder()
            .title("SAMPLE Message API")
            .description("DESCRIPTION")
            .version("VERSION")
            .termsOfServiceUrl("http://terms-of-services.url")
            .license("LICENSE")
            .licenseUrl("http://url-to-license.com")
            .build();
    }
}