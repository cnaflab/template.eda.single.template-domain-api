package com.skcc.template.eda.gobal.error.exception;

import com.skcc.template.eda.gobal.error.ErrorCode;

public class BusinessRuntimeException extends RuntimeException {

    private static final long serialVersionUID = -7099930022202674652L;

    private ErrorCode errorCode;

    public BusinessRuntimeException(String message, ErrorCode errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public BusinessRuntimeException(ErrorCode errorCode) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

}