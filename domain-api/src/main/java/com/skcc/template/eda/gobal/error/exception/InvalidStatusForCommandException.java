package com.skcc.template.eda.gobal.error.exception;

import com.skcc.template.eda.gobal.error.ErrorCode;

public class InvalidStatusForCommandException extends BusinessException {

    private static final long serialVersionUID = -5474328473401276803L;
    
    public InvalidStatusForCommandException() {
        super(ErrorCode.INVALID_STATUS_COMMAND.getMessage(), ErrorCode.INVALID_STATUS_COMMAND);
    }

    public InvalidStatusForCommandException(Object cmd) {
        super("Invalid Status for changing by Command [" + cmd.getClass().getSimpleName() + "]"
            , ErrorCode.INVALID_STATUS_COMMAND);
    }
    
    public InvalidStatusForCommandException(Object cmd, Object status) {
        super("Invalid Status for changing by Command [" + cmd.getClass().getSimpleName() + "], Current Ststus [" + ((status==null)? "null":status.toString()) + "]"
            , ErrorCode.INVALID_STATUS_COMMAND);
	}

}