package com.skcc.template.eda.gobal.object;

import java.util.HashMap;
import java.util.Map;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class GlobalResponse {

    private Map<String, Object> data;
    private Object meta;

    @Builder(builderMethodName = "of")
    private GlobalResponse(String dataName, Object data, Object meta) {
        this.data = new HashMap<String, Object>();
        this.data.put((dataName ==null || "".equals(dataName))? "dataName" : dataName, data);

        this.meta = meta;
    }
}