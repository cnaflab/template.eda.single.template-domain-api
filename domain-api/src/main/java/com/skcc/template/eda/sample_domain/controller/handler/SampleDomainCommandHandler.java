package com.skcc.template.eda.sample_domain.controller.handler;

import com.skcc.template.eda.common.sample_domain.core.object.command.CreateSampleDomainAggregateCommand;
import com.skcc.template.eda.sample_domain.core.application.object.command.FailSampleDomainAggregateCommand;
import com.skcc.template.eda.sample_domain.core.application.object.command.PlaceSampleDomainAggregateCommand;
import com.skcc.template.eda.sample_domain.core.application.object.command.SuccessSampleDomainAggregateCommand;
import com.skcc.template.eda.sample_domain.core.application.service.ISampleCommandDomainApplicationService;

import org.axonframework.commandhandling.CommandHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
@RestController
@RequestMapping("/message/sample/command")
public class SampleDomainCommandHandler {

    @Autowired
    private final ISampleCommandDomainApplicationService sampleCommandDomainApplicationService;

    @PostMapping("/handleCreateSampleDomainAggregateCommand")
    @CommandHandler
    public void handleCommand(CreateSampleDomainAggregateCommand cmd) throws Exception {
        
        sampleCommandDomainApplicationService.createAggregate( cmd );
    }

    @PostMapping("/handlePlaceSampleDomainAggregateCommand")
    @CommandHandler
    public void handleCommand(PlaceSampleDomainAggregateCommand cmd) throws Exception {
        
        sampleCommandDomainApplicationService.placeAggregate( cmd );
    }

    @PostMapping("/handleSuccessSampleDomainAggregateCommand")
    @CommandHandler
    public void handleCommand(SuccessSampleDomainAggregateCommand cmd) throws Exception {
        
        sampleCommandDomainApplicationService.successAggregate( cmd );
    }

    @PostMapping("/handleFailSampleDomainAggregateCommand")
    @CommandHandler
    public void handleCommand(FailSampleDomainAggregateCommand cmd) throws Exception {
        
        sampleCommandDomainApplicationService.failAggregate( cmd );
    }
}