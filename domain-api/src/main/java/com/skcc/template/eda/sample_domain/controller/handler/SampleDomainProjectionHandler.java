package com.skcc.template.eda.sample_domain.controller.handler;

import java.time.Instant;

import com.skcc.template.eda.common.sample_domain.core.object.event.SampleDomainAggregateEvent;
import com.skcc.template.eda.sample_domain.core.application.object.dto.SampleDomainProjctionDTO;
import com.skcc.template.eda.sample_domain.core.application.service.ISampleCommandDomainApplicationService;

import org.axonframework.eventhandling.EventHandler;
import org.axonframework.eventhandling.Timestamp;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
@RestController
@RequestMapping("/message/sample/projection")
public class SampleDomainProjectionHandler {

    private final ISampleCommandDomainApplicationService sampleCommandDomainApplicationService;

    @PostMapping
    @EventHandler
    protected void on(@RequestBody SampleDomainAggregateEvent event, @ApiParam(hidden = true) @Timestamp Instant instant ){
        
        SampleDomainProjctionDTO sampleDomainProjctionDTO = SampleDomainProjctionDTO.builder()
                                                                                .domainId(event.getId())
                                                                                .sampleData1(event.getSampleData1())
                                                                                .sampleData2(event.getSampleData2())
                                                                                .status(event.getStatus())
                                                                                .build();

        sampleCommandDomainApplicationService.projectAggregate(sampleDomainProjctionDTO);
    }
}