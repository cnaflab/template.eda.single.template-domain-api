package com.skcc.template.eda.sample_domain.controller.handler;

import java.util.List;

import com.skcc.template.eda.common.sample_domain.core.object.query.SampleDomainQueryMessage;
import com.skcc.template.eda.sample_domain.core.application.object.dto.SampleDomainQueryRequestDTO;
import com.skcc.template.eda.sample_domain.core.application.object.dto.SampleDomainQueryResponseDTO;
import com.skcc.template.eda.sample_domain.core.application.service.ISampleQueryDomainApplicationService;

import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
@RestController
@RequestMapping("/message/sample/query")
public class SampleDomainQueryHandler {

    private final ISampleQueryDomainApplicationService sampleQueryDomainApplicationService;

    @GetMapping
    @QueryHandler
    public List<SampleDomainQueryResponseDTO> on(@ModelAttribute SampleDomainQueryMessage query){
        
        SampleDomainQueryRequestDTO sampleDomainQueryRequestDTO = new SampleDomainQueryRequestDTO(
            query.getId()
            , query.getDomainId() );

        return sampleQueryDomainApplicationService.getDomainQueryData(sampleDomainQueryRequestDTO);
    }
}