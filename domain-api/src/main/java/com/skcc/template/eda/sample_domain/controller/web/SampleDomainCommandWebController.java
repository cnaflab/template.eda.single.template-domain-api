package com.skcc.template.eda.sample_domain.controller.web;

import java.util.UUID;

import com.skcc.template.eda.common.sample_domain.core.object.command.CreateSampleDomainAggregateCommand;
import com.skcc.template.eda.sample_domain.core.application.object.dto.SampleDomainCommandRequestDTO;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;


@AllArgsConstructor
@RestController
@RequestMapping("/api/sample/command")
public class SampleDomainCommandWebController {

    private final CommandGateway commandGateway;

    @PostMapping("/create")
    public ResponseEntity<Object> generateCreateSampleDomainAggregateCommand(@RequestBody SampleDomainCommandRequestDTO sampleDomainCommandRequestDTO){
        
        /**
         * 1. Sample Data Generate
         */
        {
            String id = UUID.randomUUID().toString();
            sampleDomainCommandRequestDTO = new SampleDomainCommandRequestDTO(
                id
                , "SampleData1-" + id
                , "SampleData1-" + id
                , sampleDomainCommandRequestDTO.getIsExternalError() );
        }

        /**
         * 2. Generate Aggregate Create Command
         */
        commandGateway.send( new CreateSampleDomainAggregateCommand( 
                                    sampleDomainCommandRequestDTO.getId()
                                    , sampleDomainCommandRequestDTO.getSampleData1()
                                    , sampleDomainCommandRequestDTO.getSampleData2() 
                                    , sampleDomainCommandRequestDTO.getIsExternalError() ));

        return new ResponseEntity<>(HttpStatus.OK);
    }

}