package com.skcc.template.eda.sample_domain.controller.web;

import java.util.List;

import com.skcc.template.eda.common.sample_domain.core.object.query.SampleDomainQueryMessage;
import com.skcc.template.eda.gobal.object.GlobalResponse;
import com.skcc.template.eda.sample_domain.core.application.object.dto.SampleDomainQueryRequestDTO;
import com.skcc.template.eda.sample_domain.core.application.object.dto.SampleDomainQueryResponseDTO;
import com.skcc.template.eda.sample_domain.core.application.service.ISampleQueryDomainApplicationService;

import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;


@AllArgsConstructor
@RestController
@RequestMapping("/api/sample/query")
public class SampleDomainQueryWebController {

    private final QueryGateway queryGateway;
    private final ISampleQueryDomainApplicationService sampleQueryDomainApplicationService;

    @GetMapping("/bymessage")
    public ResponseEntity<Object> querySampleDomainByMessage(@ModelAttribute SampleDomainQueryRequestDTO sampleDomainQueryRequestDTO){
        
        SampleDomainQueryMessage sampleDomainQueryMessage = new SampleDomainQueryMessage(
                                                                        sampleDomainQueryRequestDTO.getId()
                                                                        , sampleDomainQueryRequestDTO.getDomainId() );

        List<SampleDomainQueryResponseDTO> queryResult = queryGateway.query(sampleDomainQueryMessage, ResponseTypes.multipleInstancesOf(SampleDomainQueryResponseDTO.class)).join();

        return new ResponseEntity<>( queryResult, HttpStatus.OK);
    }

    @GetMapping("/byrest")
    public ResponseEntity<Object> querySampleDomainByDirect(@ModelAttribute SampleDomainQueryRequestDTO sampleDomainQueryRequestDTO){
        
        List<SampleDomainQueryResponseDTO> resultDto = sampleQueryDomainApplicationService.getDomainQueryData( sampleDomainQueryRequestDTO );


        GlobalResponse response = GlobalResponse.of()
                                    .dataName("SampleDomain")
                                    .data(resultDto)
                                    .meta(null)
                                    .build();

        return new ResponseEntity<>( response, HttpStatus.OK);
    }

    @GetMapping("/byrest1")
    public ResponseEntity<Object> querySampleDomainByDirect1(@ModelAttribute SampleDomainQueryRequestDTO sampleDomainQueryRequestDTO){
        //log.debug("[Web Controller Called] querySampleDomainByDirect");

        // HATEOAS link added
        // WebMvcLinkBuilder selfLinkBuilder = linkTo(EventController.class).slash(newEvent.getId());

        // URI createdURI = selfLinkBuilder.toUri();

        // EventResource eventResource = new EventResource(newEvent);

        // eventResource.add(linkTo(EventController.class).withRel("query-events"));
        // eventResource.add(linkTo(EventController.class).withRel("update-events"));

        // return ResponseEntity.created(createdURI).body(eventResource);

        List<SampleDomainQueryResponseDTO> aa = sampleQueryDomainApplicationService.getDomainQueryData( sampleDomainQueryRequestDTO );

        for (final SampleDomainQueryResponseDTO a : aa) {

            //Link selfLink = linkTo(methodOn(CustomerController.class).getOrderById(customerId, order.getOrderId())).withSelfRel();
        //order.add(selfLink);

            Link selfLink = WebMvcLinkBuilder.linkTo( (WebMvcLinkBuilder.methodOn(SampleDomainQueryWebController.class)).querySampleDomainByDirect(sampleDomainQueryRequestDTO) ).withSelfRel();

            a.add(selfLink);
        }

        return new ResponseEntity<>( aa , HttpStatus.OK);
    }
}