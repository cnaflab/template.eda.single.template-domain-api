package com.skcc.template.eda.sample_domain.core.application.object.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter @Setter
public class SampleDomainQueryRequestDTO {
    private String id;
    private String domainId;
}