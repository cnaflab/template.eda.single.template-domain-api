package com.skcc.template.eda.sample_domain.core.application.object.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.hateoas.RepresentationModel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "SAMPLE_DOMAIN")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter @Setter
public class SampleDomainQueryResponseDTO extends RepresentationModel<SampleDomainQueryResponseDTO> {

    @Id  
    @GeneratedValue(strategy = GenerationType.AUTO)  
    @Column(name = "ID")  
    protected long id; 
    @Column(nullable = false)
    private String domainId;
    @Column(nullable = false)
    private String sampleData1;
    @Column(nullable = false)
    private String sampleData2;
    @Column(nullable = false)
    private String status;
    
}