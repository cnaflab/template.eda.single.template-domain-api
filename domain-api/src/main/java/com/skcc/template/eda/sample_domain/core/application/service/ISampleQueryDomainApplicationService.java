package com.skcc.template.eda.sample_domain.core.application.service;

import java.util.List;

import com.skcc.template.eda.sample_domain.core.application.object.dto.SampleDomainQueryRequestDTO;
import com.skcc.template.eda.sample_domain.core.application.object.dto.SampleDomainQueryResponseDTO;

public interface ISampleQueryDomainApplicationService {

    public List<SampleDomainQueryResponseDTO> getDomainQueryData( SampleDomainQueryRequestDTO sampleDomainQueryRequestDTO );
}
