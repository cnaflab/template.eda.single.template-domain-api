package com.skcc.template.eda.sample_domain.core.application.service;

import com.skcc.template.eda.common.sample_domain.core.object.command.CreateSampleDomainAggregateCommand;
import com.skcc.template.eda.gobal.error.exception.InvalidStatusForCommandException;
import com.skcc.template.eda.sample_domain.core.application.object.command.FailSampleDomainAggregateCommand;
import com.skcc.template.eda.sample_domain.core.application.object.command.PlaceSampleDomainAggregateCommand;
import com.skcc.template.eda.sample_domain.core.application.object.command.SuccessSampleDomainAggregateCommand;
import com.skcc.template.eda.sample_domain.core.application.object.dto.SampleDomainProjctionDTO;
import com.skcc.template.eda.sample_domain.core.domain.entity.SampleDomainAggregate;
import com.skcc.template.eda.sample_domain.core.port_infra.external_system.ExternalSampleSystemPort;
import com.skcc.template.eda.sample_domain.core.port_infra.persistent.SampleDomainProjectionRepository;
import com.skcc.template.eda.sample_domain.infrastructure.external_system.ExternalSampleSystem;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.modelling.command.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Service
public class SampleCommandDomainApplicationService implements ISampleCommandDomainApplicationService {

    @Autowired
    private Repository<SampleDomainAggregate> axonRepositorySampleDomainAggregate;

    private final SampleDomainProjectionRepository sampleDomainProjectionRepository;

    private final CommandGateway commandGateway;
    
    @Override
    public void createAggregate(CreateSampleDomainAggregateCommand cmd) throws Exception {
        
        axonRepositorySampleDomainAggregate.loadOrCreate(cmd.getId(), () -> new SampleDomainAggregate(cmd))
                                        .execute(sampleDomainAggregate -> {
                                            try {
                                                sampleDomainAggregate.handle(cmd);

                                                commandGateway.send( new PlaceSampleDomainAggregateCommand( 
                                                    cmd.getId()
                                                    , cmd.getSampleData1()
                                                    , cmd.getSampleData2()
                                                    , cmd.getIsExternalError() ) );
                                            } catch (InvalidStatusForCommandException e) {
                                                //do something when InvalidStatusForCommandException occured
                                                log.error(e.getMessage());
                                            }
                                        });
        
        
    }

    @Override
    public void placeAggregate(PlaceSampleDomainAggregateCommand cmd) {
        
        axonRepositorySampleDomainAggregate.load(cmd.getId())
                                        .execute(sampleDomainAggregate -> {
                                            try {
                                                sampleDomainAggregate.handle(cmd);

                                                // if needed, do some Aggregate business Logic (e.g., change some value of Aggregate)
                                                sampleDomainAggregate.doSomeAggregateBusinessLogic();

                                                // if needed, do some Application business Logic (e.g., do interface External System)
                                                ExternalSampleSystemPort externalSampleSystemPort = new ExternalSampleSystem();
                                                boolean result = externalSampleSystemPort.doExternalSampleSystemLogic(cmd.getIsExternalError());

                                                // depending on Result of business Logic, Publish the proper Command for next step
                                                if(result){
                                                    commandGateway.send( new SuccessSampleDomainAggregateCommand( 
                                                        cmd.getId()
                                                        , cmd.getSampleData1()
                                                        , cmd.getSampleData2()
                                                        , cmd.getIsExternalError() ) );
                                                } else {
                                                    commandGateway.send( new FailSampleDomainAggregateCommand( 
                                                        cmd.getId()
                                                        , cmd.getSampleData1()
                                                        , cmd.getSampleData2()
                                                        , cmd.getIsExternalError() ) );
                                                }
                                                
                                            } catch (InvalidStatusForCommandException e) {
                                                //do something when InvalidStatusForCommandException occured
                                                log.error(e.getMessage());
                                            }
                                        });
    }

    @Override
    public void successAggregate(SuccessSampleDomainAggregateCommand cmd) {
        
        axonRepositorySampleDomainAggregate.load(cmd.getId())
                                        .execute(sampleDomainAggregate -> {
                                            try {
                                                sampleDomainAggregate.handle(cmd);
                                                
                                            } catch (InvalidStatusForCommandException e) {
                                                //do something when InvalidStatusForCommandException occured
                                                log.error(e.getMessage());
                                            }
                                        });
    }

    @Override
    public void failAggregate(FailSampleDomainAggregateCommand cmd) {
        
        axonRepositorySampleDomainAggregate.load(cmd.getId())
                                        .execute(sampleDomainAggregate -> {
                                            try {
                                                sampleDomainAggregate.handle(cmd);
                                                
                                            } catch (InvalidStatusForCommandException e) {
                                                //do something when InvalidStatusForCommandException occured
                                                log.error(e.getMessage());
                                            }
                                        });
    }

    @Override
    public void projectAggregate(SampleDomainProjctionDTO sampleDomainProjctionDTO){
        
        sampleDomainProjectionRepository.save(sampleDomainProjctionDTO);
    }
}
