package com.skcc.template.eda.sample_domain.core.application.service;

import java.util.List;

import com.skcc.template.eda.sample_domain.core.application.object.dto.SampleDomainQueryRequestDTO;
import com.skcc.template.eda.sample_domain.core.application.object.dto.SampleDomainQueryResponseDTO;
import com.skcc.template.eda.sample_domain.core.port_infra.persistent.SampleDomainQueryRepository;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class SampleQueryDomainApplicationService implements ISampleQueryDomainApplicationService {

    private final SampleDomainQueryRepository sampleDomainQueryRepository;

    @Override
    public List<SampleDomainQueryResponseDTO> getDomainQueryData( SampleDomainQueryRequestDTO sampleDomainQueryRequestDTO ){
        
        return sampleDomainQueryRepository
                    .findAllByDomainId(sampleDomainQueryRequestDTO.getDomainId());
    }
}
