package com.skcc.template.eda.sample_domain.core.port_infra.external_system;

public interface ExternalSampleSystemPort {

    public boolean doExternalSampleSystemLogic(int isExternalError);

}