package com.skcc.template.eda.sample_domain.core.port_infra.persistent;

import com.skcc.template.eda.sample_domain.core.application.object.dto.SampleDomainProjctionDTO;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SampleDomainProjectionRepository extends JpaRepository<SampleDomainProjctionDTO, String> {
}