package com.skcc.template.eda.sample_domain.core.port_infra.persistent;

import java.util.List;
import java.util.Optional;

import com.skcc.template.eda.sample_domain.core.application.object.dto.SampleDomainQueryResponseDTO;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SampleDomainQueryRepository extends JpaRepository<SampleDomainQueryResponseDTO, String> {

    Optional<SampleDomainQueryResponseDTO> findByDomainId(String domainId);
    List<SampleDomainQueryResponseDTO> findAllByDomainId(String domainId);
}