package com.skcc.template.eda.sample_domain.infrastructure.external_system;

import com.skcc.template.eda.sample_domain.core.port_infra.external_system.ExternalSampleSystemPort;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExternalSampleSystem implements ExternalSampleSystemPort{

    @Override
    public boolean doExternalSampleSystemLogic(int isExternalError){
        
        // do some interface logic with external system

        boolean result = !(isExternalError == 1);

        if(result) log.debug("[ExternalSampleSystem result] Success");
        else log.debug("[ExternalSampleSystem result] Fail");
        
        return result;
    }

}