package com.skcc.template.eda.sample_domain.infrastructure.persistent.axon;

import com.skcc.template.eda.sample_domain.core.domain.entity.SampleDomainAggregate;

import org.axonframework.modelling.command.Repository;
import org.axonframework.spring.config.AxonConfiguration;
import org.springframework.context.annotation.Bean;

public class AxonRepositorySampleDomainAggregate {

    @Bean
    Repository<SampleDomainAggregate> accessorAggregateRootRepository(AxonConfiguration axonConfiguration) {
       
        return axonConfiguration.repository(SampleDomainAggregate.class);
    }
    
}