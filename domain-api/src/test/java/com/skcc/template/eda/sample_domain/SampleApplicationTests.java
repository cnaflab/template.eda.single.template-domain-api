package com.skcc.template.eda.sample_domain;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles(profiles = "local")
@SpringBootTest(properties = "spring.config.location=" +
            "classpath:/config/application/"
            )
            
class SampleApplicationTests {

    @Test
    public void testSampleDomainCommand() {
    }

}
