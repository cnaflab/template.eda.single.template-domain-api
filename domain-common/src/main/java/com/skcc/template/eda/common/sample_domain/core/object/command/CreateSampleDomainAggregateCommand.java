package com.skcc.template.eda.common.sample_domain.core.object.command;

import org.axonframework.modelling.command.TargetAggregateIdentifier;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
@Getter
public class CreateSampleDomainAggregateCommand {

  @TargetAggregateIdentifier
  private String id;
  private String sampleData1;
  private String sampleData2;
  int isExternalError;
}